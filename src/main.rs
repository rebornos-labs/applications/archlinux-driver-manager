use archlinux_driver_manager::commandline::*;

fn main() {
    let archlinux_driver_manager_app = CommandlineInterface::new();
    archlinux_driver_manager_app.run();
}
